#!/usr/bin/env bash

# Update
dnf check-update
dnf update -y
dnf install -y dnf-plugins-core wget curl

dnf config-manager \
    --add-repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

# Add required dependencies for the jenkins package
dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
dnf install -y chkconfig java-devel maven docker-ce docker-ce-cli containerd.io
# dnf upgrade
# dnf list docker-ce --showduplicates | sort -r
systemctl start docker
systemctl enable docker
systemctl status docker

# Jenkins
dnf install -y jenkins
#JENKINS_PORT="8080"
sed -i.default -e '/^JENKINS_PORT/cJENKINS_PORT="8282"' /etc/sysconfig/jenkins
sysctl net.ipv4.ip_forward=1
usermod -a -G docker vagrant
usermod -a -G docker jenkins

systemctl daemon-reload
systemctl start jenkins
systemctl status jenkins
